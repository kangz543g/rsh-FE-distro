# Installation instructions

## Install NPM

`curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -`

`sudo apt-get install -y nodejs`

## Install process manager [PM2](https://github.com/Unitech/pm2)

`npm install -g pm2`

## Install project dependencies

Navigate to the folder where the application was unpacked. Once there execute:
`npm install`

# Managing instance with PM2

To start the application:

`pm2 start server.production.js --name shakefe`

In this instance 'shakefe' is the name we will use to keep track of our front-end.

Once started we can control the instance using following commands:

**Restarting**

`pm2 restart shakefe`

**Stopping**

`pm2 stop shakefe`
